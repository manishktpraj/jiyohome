import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,MenuController,Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ListPage } from '../pages/list/list';
import { MyaddressPage } from '../pages/myaddress/myaddress';
import { OffersPage } from '../pages/offers/offers';
import { MycartPage } from '../pages/mycart/mycart';
import { SharearnPage } from '../pages/sharearn/sharearn';
import { WalletPage } from '../pages/wallet/wallet';
import { ProductlistPage } from '../pages/productlist/productlist';
import { TermconditionPage } from '../pages/termcondition/termcondition';

import { InAppBrowser } from '@ionic-native/in-app-browser';



import { RegisterphonenumberPage } from '../pages/registerphonenumber/registerphonenumber';
import { AccountPage } from '../pages/account/account';
import { MyorderPage } from '../pages/myorder/myorder';

import { ServicesProvider } from '../providers/services/services';
import { Storage } from '@ionic/storage';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
	  @ViewChild(Nav) nav: Nav;
	  rootPage: any = HomePage;
	  pages: Array<{title: string, component: any}>;
		user_data:any={};
		
		credentials : any = {};
		dashboarddata:any={};

  constructor(public platform: Platform,
	private iab: InAppBrowser,
	 public statusBar: StatusBar, public splashScreen: SplashScreen,private menu:MenuController,public services:ServicesProvider,public storage:Storage,private events:Events) {
    this.initializeApp();
			// used for an example of ngFor and navigation
			this.pages = [
			  { title: 'Home', component: HomePage },
			  { title: 'List', component: ListPage }
			];

 events.subscribe('user_jiyo:created', (user, time) => {
    // user and time are the same arguments passed in `events.publish(user, time)`
	
	if(user==null)
	{
		this.user_data ={};
	}else{
				  this.user_data = user;
	}	  
				  				  	console.log(this.user_data,"event");  

  });
	this.storage.get('user_jiyo').then(data => {
				if(data!=null)
				{
				  this.user_data = data;
				  	console.log(this.user_data,"user_data");  
				}
				 });
  
				this.services.getparentcategory(this.credentials).then((result: any) => {
 				this.dashboarddata=result;
				});
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
   // this.nav.setRoot(page.component);
  }
  
    goToRegMobNo() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
	this.menu.close();
    this.nav.push(RegisterphonenumberPage);
  }
  
   goToProductlistPage(category) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
	this.menu.close();
    this.nav.push(ProductlistPage,{category:category});
	}
  
  openmyac() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
	
	this.storage.get('user_jiyo').then(data => {
				if(data!=null)
				{
						this.menu.close();
						this.nav.push(AccountPage); 
				}
				else{
					this.menu.close();
						this.nav.push(RegisterphonenumberPage);
				}
				 });	  

  }
  
  openmyorder() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
	this.storage.get('user_jiyo').then(data => {
				if(data!=null)
				{
						this.menu.close();
						this.nav.push(MyorderPage);
				}
				else{
					this.menu.close();
						this.nav.push(RegisterphonenumberPage);
				}
				 });	  
  }
  
  openmycart() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
		this.storage.get('user_jiyo').then(data => {
	if(data!=null)
				{
						this.menu.close();
						this.nav.push(MycartPage);
				}
				else{
					this.menu.close();
						this.nav.push(RegisterphonenumberPage);
				}
				 });	  
	
  }
  openmyaddress() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
		this.storage.get('user_jiyo').then(data => {
	if(data!=null)
				{
						this.menu.close();
						this.nav.push(MyaddressPage);
				}
				else{
					this.menu.close();
						this.nav.push(RegisterphonenumberPage);
				}
				 });	  
  }
  
  opensharearn() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
		this.storage.get('user_jiyo').then(data => {
	if(data!=null)
				{
						this.menu.close();
						this.nav.push(SharearnPage);
				}
				else{
					this.menu.close();
						this.nav.push(RegisterphonenumberPage);
				}
				 });	  
  }
  
  openwallet() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
		this.storage.get('user_jiyo').then(data => {
	if(data!=null)
				{
						this.menu.close();
						this.nav.push(WalletPage);
				}
				else{
					this.menu.close();
						this.nav.push(RegisterphonenumberPage);
				}
				 });	  
  }
  
  openrate()
  {
		  const browser = this.iab.create('https://play.google.com/store/apps/details?id=medico.com.codexo&hl=en','_system','loction=no');
  
  }
  
  
}

