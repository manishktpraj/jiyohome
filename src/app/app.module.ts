import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { OffersPage } from '../pages/offers/offers';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HelpsupportPage } from '../pages/helpsupport/helpsupport';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ListPage } from '../pages/list/list';
import { AccountPage } from '../pages/account/account';
import { EditprofilePage } from '../pages/editprofile/editprofile';

import { ViewonePage } from '../pages/viewone/viewone';
import { ViewtwoPage } from '../pages/viewtwo/viewtwo';
import { ViewbrandPage } from '../pages/viewbrand/viewbrand';
import { FaqPage } from '../pages/faq/faq';
import { PaymentPage } from '../pages/payment/payment';
import { OrderdetailsPage } from '../pages/orderdetails/orderdetails';
import { ThankyouPage } from '../pages/thankyou/thankyou';


import { SocialSharing } from '@ionic-native/social-sharing';



import { MycartPage } from '../pages/mycart/mycart';
import { WalletPage } from '../pages/wallet/wallet';
import { MyorderPage } from '../pages/myorder/myorder';
import { AddressPage } from '../pages/address/address';
import { MyaddressPage } from '../pages/myaddress/myaddress';
import { AddaddressPage } from '../pages/addaddress/addaddress';
import { SharearnPage } from '../pages/sharearn/sharearn';
import { NotificationPage } from '../pages/notification/notification';
import { ProductlistPage } from '../pages/productlist/productlist';
import { BrandlistPage } from '../pages/brandlist/brandlist';
import { TermconditionPage } from '../pages/termcondition/termcondition';

import { ProductdetailsPage } from '../pages/productdetails/productdetails';

import { LoginPage } from '../pages/login/login';
import { RegisterphonenumberPage } from '../pages/registerphonenumber/registerphonenumber';
import { OtpPage } from '../pages/otp/otp';
import { RegisterPage } from '../pages/register/register';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServicesProvider } from '../providers/services/services';
import { IonicStorageModule } from '@ionic/storage';
import { StatePage } from '../pages/state/state';
import { CityPage } from '../pages/city/city';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
	LoginPage,
	AccountPage,
	MycartPage,
	MyorderPage,
	MyaddressPage,
	SharearnPage,
	NotificationPage,
	AddaddressPage,
	ProductlistPage,
	ProductdetailsPage,
	RegisterPage,
	BrandlistPage,
	EditprofilePage,
	ViewtwoPage,
	ViewbrandPage,
	OtpPage,
	WalletPage,
OrderdetailsPage,
	ViewonePage,
	TermconditionPage,
	HelpsupportPage,
	FaqPage,

	TermconditionPage,
	RegisterphonenumberPage,
	AddressPage,OffersPage,StatePage,CityPage,PaymentPage,ThankyouPage
  ],
  imports: [
    BrowserModule,
	HttpClientModule,
    IonicModule.forRoot(MyApp),
	    IonicStorageModule.forRoot()

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
	AccountPage,
	MycartPage,
	MyorderPage,
	LoginPage,
	FaqPage,
	MyaddressPage,
	SharearnPage,
	EditprofilePage, 
	NotificationPage,
	AddaddressPage,
	ProductlistPage,
	ProductdetailsPage,
	RegisterPage,
	TermconditionPage,
	OtpPage,
	ViewbrandPage,
	WalletPage,
	OrderdetailsPage,
	HelpsupportPage,
	ViewonePage,
	ViewtwoPage,
	OffersPage,
	BrandlistPage,
	RegisterphonenumberPage,
	AddressPage,StatePage,CityPage,PaymentPage,ThankyouPage
  ],
  providers: [
    StatusBar,

	SplashScreen,
	InAppBrowser,

    SplashScreen,

	SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServicesProvider
  ]
})
export class AppModule {}
