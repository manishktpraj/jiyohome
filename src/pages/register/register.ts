import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ToastController,LoadingController,Events } from 'ionic-angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { HomePage } from '../home/home';


/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
			user_data:any={};

			validations_form:any;
			user_id : any =0 ;
			loginErrorMsg:any='';
		 
			reff_enable:boolean=false;

	

 constructor(private services: ServicesProvider ,public navCtrl: NavController, public navParams: NavParams,private toastController: ToastController, private alertcontroller:AlertController, public formbuilder : FormBuilder, public loadingCtrl : LoadingController, private events:Events) {
	 
	 	 this.user_data = this.navParams.get("user_data");
	  
	   this.validations_form = this.formbuilder.group({
		  
		  user_first_name:new FormControl( '', Validators.required),
		  user_last_name:new FormControl( '', Validators.required),
		  user_email_id:new FormControl( '', Validators.required),
		  user_mobile : new FormControl( '', Validators.required),
		  user_password : new FormControl( '', Validators.required),
		  user_ref_id : new FormControl('')
		  
		  
	  });
	  this.validations_form.patchValue({
	  
	  user_mobile : this.user_data.user_mobile,
	
	  });

  }

		
  
  
  goforRegister(){  
let credentials =  this.validations_form.value;  
let phoneno = /^\d{10}$/;

this.loginErrorMsg = '';
if(credentials['user_first_name']=='')
{
this.loginErrorMsg = 'Please Enter Firstname';
return;
}else if(credentials['user_last_name']=='')
{
this.loginErrorMsg = 'Please Enter Lastname';
return;
}else if(credentials['user_mobile']=='')
{
this.loginErrorMsg = 'Please Enter Mobile Number';
return;
}else if((credentials['user_mobile'].length < 10 || credentials['user_mobile'].length > 10 || !credentials['user_mobile'].match(phoneno))) {
this.loginErrorMsg = "Please enter 10 digit phone number";
return;
}else if(credentials['user_email_id']=='')
{
this.loginErrorMsg = 'Please Enter Email';
return;
} else if(credentials['user_password']=='')
{
this.loginErrorMsg = 'Please Enter Password';
return;
} 
if(credentials['user_password'].length < 6 || credentials['user_password'].length > 12)
{
this.loginErrorMsg = 'Please Enter 6 digits Password';
return;
} 
credentials['user_id'] = this.user_data.user_id;

if(this.reff_enable){
if(credentials['user_ref_id']=='')
{
this.loginErrorMsg = 'Please Enter Refer code';
return;
} 
}
 this.loginErrorMsg = "";
 let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
    loading.present();
 this.services.finalregis(credentials).then((result: any) => {
	 console.log(result,"");
	 
 if(result.message=='ok')
{
 //action needed
 	   this.events.publish('user_jiyo:created', result.results, Date.now());

 	 this.services.saveuser(result.results);
		this.navCtrl.setRoot(HomePage);
 
}else{
	this.loginErrorMsg = 'wrong Username or Password';     
}

loading.dismiss();
}, (err) => {
console.log(err);
loading.dismiss();
});  
  
}  
  
getenablereff(ev:any)
{
	console.log(ev.checked);
if(ev.checked)
{
this.reff_enable =true;	
}else{
this.reff_enable =false;		
}	
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  
}

