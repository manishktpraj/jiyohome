import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController,ViewController} from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';




/**
 * Generated class for the StatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-state',
  templateUrl: 'state.html',
})
export class StatePage {
	states_list:any=[];
	preloaded_list:any=[];
	selected_state:any={};


  constructor(private services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams,
	public modalCtrl: ModalController,public viewCtrl: ViewController) {
	
		this.states_list =this.navParams.get('state_data');
		this.preloaded_list =this.states_list;
		console.log(this.states_list,'this.states_list');  
  }


   
  selectSearchResult(itemdata) 
  {
	  this.selected_state = itemdata.state_name;
	console.log('this.states',this.selected_state);
		this.dismiss();
	  
  }
  dismiss() {
   this.viewCtrl.dismiss(this.selected_state);
  }
   getItems(ev: any) 
 {
	  let searchdata:any= ev.target.value;
	  this.states_list=[];
	  this.preloaded_list.forEach( (item, index)=> {
								 if(item.state_name.toUpperCase().indexOf(searchdata.toUpperCase()) > -1) {
									  this.states_list.push(item);
								}
							
						});
	  
  }
  onCancel()
  {
	  this.states_list=this.preloaded_list;
	  
	  
  }
}
