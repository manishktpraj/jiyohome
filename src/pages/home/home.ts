import { Component } from '@angular/core';
import { NotificationPage } from '../notification/notification';
import { MycartPage } from '../mycart/mycart';
import { ProductlistPage } from '../productlist/productlist';
import { BrandlistPage } from '../brandlist/brandlist';
import { StatusBar } from '@ionic-native/status-bar';

import { ViewonePage } from '../viewone/viewone';
import { ViewtwoPage } from '../viewtwo/viewtwo';
import { ViewbrandPage } from '../viewbrand/viewbrand';

import { ProductdetailsPage } from '../productdetails/productdetails';
import { NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
 	user_id : any =0 ;
	user_data:any={};
	credentials : any = {};
	dashboarddata:any={};
	cartdata:any=[];
	
	
  constructor(private services: ServicesProvider,public statusBar:StatusBar ,public navCtrl: NavController,public navParams: NavParams,public storage:Storage) {
	  
	    statusBar.show();
      statusBar.overlaysWebView(false);
      statusBar.backgroundColorByHexString('#263343');
       statusBar.styleLightContent();
		this.user_id = this.navParams.get("user_id");
		this.user_data = this.navParams.get("user_datat");
		
		this.services.homedashboard(this.credentials).then((result: any) => {
				console.log(result,"");
				this.dashboarddata=result;
				});
  
  }		

ionViewWillEnter()
{	
this.storage.get('user_jiyo_cart').then(data => {
				if(data!=null)
				{
				  this.cartdata = data;
				  console.log(this.cartdata,"this.cartdata");
 				}
				 });	  
	
}  
			opennotification()
			{
			this.navCtrl.push(NotificationPage);
			}
			opencart()
			{
			this.navCtrl.push(MycartPage);
			}
			
			openproductdetail(product)
			{
			this.navCtrl.push(ProductdetailsPage,{product_id:product.product_id});
			}
			
			openproductlist(category)
			{
			this.navCtrl.push(ProductlistPage,{category:category});
			}
			
			openbrandlist(category)
			{
			this.navCtrl.push(ProductlistPage,{brand:category});
			}
			
			goViewOne()
			{
			this.navCtrl.push(ViewonePage);
			}
			goViewTwo(category)
			{
			this.navCtrl.push(ProductlistPage,{tag:category});
			}
			
			goViewBrand()
			{
			this.navCtrl.push(ViewbrandPage);
			}
			

 	getpercentagediscount(pdd)
	{
		let ev:any = 0;
		 let discountp = (( pdd.sk_productbusinessprice[ev].pu_selling_price - pdd.sk_productbusinessprice[ev].pu_net_price)/pdd.sk_productbusinessprice[ev].pu_selling_price)*100;
		 return discountp.toFixed(0);
 
	}	
}
