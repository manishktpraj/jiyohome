import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the TermconditionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-termcondition',
  templateUrl: 'termcondition.html',
})
export class TermconditionPage {
my_iframe_url:any='';
  my_url:any='https://www.jiyohome.com/terms-conditions';
   hidef:any=0;

  constructor(public navCtrl: NavController, public navParams: NavParams,private sanitize: DomSanitizer) {
        this.my_iframe_url =  this.sanitize.bypassSecurityTrustResourceUrl(this.my_url);
setTimeout(()=>{
			this.hidef=1;
		},10000); 

 }


}
