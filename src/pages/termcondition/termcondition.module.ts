import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermconditionPage } from './termcondition';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(TermconditionPage),
  ],
})
export class TermconditionPageModule {}
