import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ToastController,LoadingController,Events } from 'ionic-angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';

/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
	
	loginErrorMsg:any='';
	
	validations_form:any;
	user_id : any =0 ;
	user_mobile : any ='' ;

   constructor(private services: ServicesProvider ,public navCtrl: NavController, public navParams: NavParams,private toastController: ToastController, private alertcontroller:AlertController, public formbuilder : FormBuilder, public loadingCtrl : LoadingController,private events:Events) {
	   
	 		this.user_mobile = this.navParams.get("user_mobile");
			
	 		this.user_id = this.navParams.get("user_id");
  
			this.validations_form = this.formbuilder.group({
	  
			user_otp : new FormControl( '', Validators.required)
});
  }
  
otpcheck(){  
let credentials =  this.validations_form.value;  
 let phoneno = /^\d{4}$/;
 
 
this.loginErrorMsg = '';
 credentials['user_id']= this.user_id;
 
if(credentials['user_otp']=='')
{
this.loginErrorMsg = 'Please Enter Mobile Number';
return;
}else if((credentials['user_otp'].length < 4 || credentials['user_otp'].length > 4 || !credentials['user_otp'].match(phoneno))) {
this.loginErrorMsg = "Please enter 4 digit OTP";
return;
}else{
this.loginErrorMsg = "";
	
	
	let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  
    loading.present();
  
 this.services.otpcheck(credentials).then((result: any) => {
	 console.log(result,"");
	
	 

	 if(result.message=='ok')
{
	
 //action needed
 if(result.results.user_status==1)
 {
	 
	   this.events.publish('user_jiyo:created', result.results, Date.now());

	 this.services.saveuser(result.results);
	this.navCtrl.setRoot(HomePage);
 
 }else{
	 
	 this.navCtrl.push(RegisterPage, {user_data:result.results})

 }
 
 
}else{
this.loginErrorMsg = 'wrong Username or Password';     
}

loading.dismiss();
}, (err) => {
console.log(err);
loading.dismiss();
});  

}

}
  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }

}
