import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { ProductdetailsPage } from '../productdetails/productdetails';


/**
 * Generated class for the ViewtwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewtwo',
  templateUrl: 'viewtwo.html',
})
export class ViewtwoPage {
	
	tag:any={};
	product:any=[];
	product_url:any='';
	
  constructor(private services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams) {
	  
	    this.tag=this.navParams.get('tag_data');
 	  
	  
  }
  openproductdetail()
			{
			this.navCtrl.push(ProductdetailsPage);
			}
	
  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewtwoPage');
  } ionViewWillEnter() {
   let cred ={product_tag:this.tag.tag_id};
				 	this.services.viewcategory(cred).then((result: any) => {				
			 if(result.message!=undefined)
			 {
			 this.product=result.result;
			 	this.product_url=result.url;
			 	}
				 }, (err) => {
								console.log(err);
								//loading.dismiss();
								});  
  }
setdata(ev:any,product){
console.log(ev,'masd');
console.log(product,'masd');

 let classname ='clscommon'+product.product_id;
 let x = document.getElementsByClassName(classname);
 console.log(classname,"classname");
 let p = (( product.sk_productbusinessprice[ev].pu_selling_price - product.sk_productbusinessprice[ev].pu_net_price)/product.sk_productbusinessprice[ev].pu_selling_price)*100;
 
x[0].innerHTML='<span class="new-price">₹'+product.sk_productbusinessprice[ev].pu_net_price+'</span> <span class="old-price ml-5">₹'+product.sk_productbusinessprice[ev].pu_selling_price+'</span> <span class="discount ml-5">'+p.toFixed(0)+'% off</span>';
		
}
}
