import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewtwoPage } from './viewtwo';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ViewtwoPage),
  ],
})
export class ViewtwoPageModule {}
