import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';
import { OrderdetailsPage } from '../orderdetails/orderdetails';
/**
 * Generated class for the MyorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myorder',
  templateUrl: 'myorder.html',
})
export class MyorderPage {

  orders:any=[];
  user_data:any={};
   constructor(public navCtrl: NavController, public navParams: NavParams,
    private services:ServicesProvider,private storage:Storage,private loadingCtrl:LoadingController) {
      this.getuserdata();
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad MyordersPage');
    }
    
    
   getuserdata()
   {
    this.storage.get('user_jiyo').then(data => {
      if(data!=null)
      {
  
      this.user_data= data;	  
      this.loadorders();
  
      }
      });
   }
  loadorders()
  {
    let credentials = {user_id:this.user_data.user_id};  
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
      });
      
      loading.present();
      
   this.services.orders(credentials).then((result: any) => {
  loading.dismiss();	
  if(result.message=='ok')
  {
  this.orders = 	result.result; 	
  }
  }, (err) => {
  console.log(err);
  loading.dismiss();
  });	
    
  }
  getitemname(itemd)
  {
    let t=[];
    itemd.forEach((Item)=>{
    t.push(Item.td_name);
    });
    return t.join(', ');
  }
openorderdetail()
{
	this.navCtrl.push(OrderdetailsPage);
}
}
