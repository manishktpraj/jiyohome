import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ToastController,LoadingController,ModalController } from 'ionic-angular';
import {FormBuilder, Validators, FormControl,FormGroup} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';
import { StatePage } from '../state/state';
import { CityPage } from '../city/city';


/**
 * Generated class for the AddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})
export class AddressPage {
		user_data : any ={};
		user_d : any ={};
		loginErrorMsg:any='';
		validations_form:FormGroup;
		active_type:any='Home';
selected_city:any='';
selected_state:any='';
state_list:any=[];
  constructor(private services: ServicesProvider ,public navCtrl: NavController, public navParams: NavParams,private toastController: ToastController,
	 private alertcontroller:AlertController, public formbuilder : FormBuilder,
	  public loadingCtrl : LoadingController, public storage:Storage,public modalCtrl:ModalController) {
  this.state_list =this.navParams.get('states_list');
  console.log(this.state_list ,"this.state_list ");
  if(this.navParams.get('address_data')!=undefined)
  {
     
		this.user_d = this.navParams.get('address_data');
			console.log(this.user_d,"this.user_d")
	  
	  if(this.user_d.ab_type!='')
	{
		 this.active_type = this.user_d.ab_type;
	}
	   this.validations_form = this.formbuilder.group({
	  
	  ab_name:new FormControl(  this.user_d.ab_name, Validators.required),
	  ab_phone:new FormControl(  this.user_d.ab_phone, Validators.required),
	  ab_address:new FormControl(  this.user_d.ab_address, Validators.required),
	  ab_locality:new FormControl( this.user_d.ab_locality, Validators.required),
	  ab_landmark:new FormControl(  this.user_d.ab_landmark, Validators.required),
	  ab_city : new FormControl( this.user_d.ab_city, Validators.required),
	  ab_state : new FormControl( this.user_d.ab_state, Validators.required),
	  ab_pincode : new FormControl( this.user_d.ab_pincode, Validators.required),
	  ab_type:  this.active_type

	  
	
	  });
	  
		
		}else{
		
		
     this.validations_form = this.formbuilder.group({
	  
	  ab_name:new FormControl( '', Validators.required),
	  ab_phone:new FormControl( '', Validators.required),
	  ab_address:new FormControl( '', Validators.required),
	  ab_locality:new FormControl( '', Validators.required),
	  ab_landmark:new FormControl( '', Validators.required),
	  ab_city : new FormControl( '', Validators.required),
	  ab_state : new FormControl( '', Validators.required),
	  ab_pincode: new FormControl( '', Validators.required),
	  ab_type: new FormControl(this.active_type),

	 
	  });
		}
	    this.storage.get('user_jiyo').then(data => {
		if(data!=null)
		{
		this.user_data= data;	  
		}
		});

	  
	  
  }
  
  activetype(strtype)
{
	this.active_type=strtype;
	
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddressPage');
  }
  
filladdress(){  
let credentials =  this.validations_form.value;  
 let phoneno = /^\d{10}$/;


this.loginErrorMsg = '';
console.log(credentials,'credentials');
if(credentials['ab_name']=='')
{
this.loginErrorMsg = 'Please Enter Name';
return;
}else if(credentials['ab_phone']=='' || credentials['ab_phone']==null) 
{
this.loginErrorMsg = 'Please Enter Mobile Number';
return;
}else if((credentials['ab_phone'].length < 10 || credentials['ab_phone'].length > 10 || !credentials['ab_phone'].match(phoneno))) {
this.loginErrorMsg = "Please enter 10 digit phone number";
return;
}
else if(credentials['ab_address']=='')
{
this.loginErrorMsg = 'Please Enter Flat or House Number';
return;
}else if(credentials['ab_locality']=='')
{
this.loginErrorMsg = 'Please Enter Apartment of Locality Name';
return;
}else if(credentials['ab_landmark']=='')
{
this.loginErrorMsg = 'Please Enter Colony Name';
return;
}else if(credentials['ab_city']=='')
{
this.loginErrorMsg = 'Please Enter City';
return;
}else if(credentials['ab_state']=='')
{
this.loginErrorMsg = 'Please Enter State';
return;
}
credentials['ab_type'] = this.active_type;

this.loginErrorMsg = "";
 let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
    loading.present();
   credentials['ab_user_id'] =this.user_data.user_id;
   if(this.user_d!=null)
   {
	 credentials['ab_id'] =  this.user_d.ab_id;  
   }
   

 this.services.saveaddress(credentials).then((result: any) => {
	 console.log(result,"");
	 if(result.message!=undefined)
				{
			 this.navCtrl.pop();
				}
loading.dismiss();
}, (err) => {
console.log(err);
loading.dismiss();
});  

  }
opencity()
{
	if(this.selected_state != '')
	{
		let profileModal = this.modalCtrl.create(CityPage, { state_data: this.selected_state});
		profileModal.onDidDismiss(data => {
			if(data!=undefined && data!=null)
			{
			   this.selected_city = data;
				  console.log(this.selected_city,"this.selected_city");
			  let credentials = this.validations_form.value;
			  credentials['ab_city'] = this.selected_city;
			  this.validations_form.setValue(credentials);
		 
			   
			}
	 });
		profileModal.present(); 
		
	}else{
		
     this.services.presentToast('Please Select State');	
	}
	
}
openstate()
{
	
//	let modal = this.modalctrl.create(,{});
	///modal.dis(
	///if()
	///modal.present()

	let profileModal = this.modalCtrl.create(StatePage, { state_data: this.state_list});
	profileModal.onDidDismiss(data => {
		if(data!=undefined && data!=null)
		{
		   this.selected_state = data;
		  	console.log(this.selected_state,"this.selected_address");
		  let credentials = this.validations_form.value;
		  credentials['ab_state'] = this.selected_state;
		  this.validations_form.setValue(credentials);
	 
		   
		}
 });
	profileModal.present(); 
	
}
}
