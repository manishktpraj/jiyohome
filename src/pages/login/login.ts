import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ToastController,LoadingController } from 'ionic-angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { RegisterphonenumberPage } from '../registerphonenumber/registerphonenumber';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPagfe page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginErrorMsg:any='';
	
	validations_form:any;

  constructor(private services: ServicesProvider ,public navCtrl: NavController, public navParams: NavParams,private toastController: ToastController, private alertcontroller:AlertController, public formbuilder : FormBuilder, public loadingCtrl : LoadingController) {
	  
	   this.validations_form = this.formbuilder.group({
	  
	 username:new FormControl( '', Validators.required),
	 passwordd : new FormControl( '', Validators.required)
  });

  }

  goToRegMobNo(){
	  this.navCtrl.push(RegisterphonenumberPage)
  }
   goToRegister(){
	  this.navCtrl.push(RegisterPage)
  }
  goToHome(){
  this.navCtrl.setRoot(HomePage)
  }
  
checkmobilenumber(){  
let credentials =  this.validations_form.value;  
this.loginErrorMsg = '';
if(credentials['username']=='')
{
this.loginErrorMsg = 'Please Enter Email id/Mobile Number';
return;
} if(credentials['passwordd']=='')
{
this.loginErrorMsg = 'Please Enter Password';
return;
}  

 let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  
    loading.present();
  
 this.services.loginuser(credentials).then((result: any) => {
	 console.log(result,"");
 if(result.message=='ok')
{
 //action needed
		this.services.saveuser(result.results);
		this.navCtrl.setRoot(HomePage)

}else{
this.loginErrorMsg = 'wrong Username or Password';     
}

loading.dismiss();
}, (err) => {
console.log(err);
loading.dismiss();
});  

}  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
