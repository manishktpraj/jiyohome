import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { ProductdetailsPage } from '../productdetails/productdetails';
import { Storage } from '@ionic/storage';
import { MycartPage } from '../mycart/mycart';



/**
 * Generated class for the ProductlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productlist',
  templateUrl: 'productlist.html',
})
export class ProductlistPage {
	category:any={};
	brand:any={};
	tag:any={};
	product:any=[];
	orginal_product:any=[];
	product_url:any='';
	test:boolean =false;
cartdata:any=[];
activeindex:any=[];
  constructor(private services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams,public loadingCtrl : LoadingController,public storage:Storage) {
	  if(this.navParams.get('category')!=undefined)
	  {
	  this.category=this.navParams.get('category');
	  }
	    if(this.navParams.get('brand')!=undefined)
	  {
	  this.brand=this.navParams.get('brand');
	  console.log( this.brand," this.brand");
	  }    if(this.navParams.get('tag')!=undefined)
	  {
	  this.tag=this.navParams.get('tag');
	  console.log( this.tag," this.tag");
	  }
	  console.log(this.category);
	 this.productcategory();
	  	this.storage.get('user_jiyo_cart').then(data => {
				if(data!=null)
				{
				  this.cartdata = data;
				  console.log(this.cartdata,"this.cartdata");
 				}
				 });	  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductlistPage');
  }
  
  
		openproductdetail(product)
					{
					this.navCtrl.push(ProductdetailsPage,{product_id:product.product_id});
					}
					
	productcategory()
				{
					let loading = this.loadingCtrl.create({
						content: 'Please wait...'
					  });
					  
						loading.present();
						let cred 
							if(this.category.category_id!=undefined)
							{								
								cred={category_id:this.category.category_id};
							}else if(this.brand.brand_id!=undefined)
							{
						  cred ={brand_id:this.brand.brand_id};
							
							}else if(this.tag.tag_id!=undefined)
							{
						  cred ={tag_id:this.tag.tag_id};
							
							}
					
					console.log(cred);
					
						this.services.productcategory(cred).then((result: any) => {
				console.log(result,"");
				
				if(result.message!=undefined)
				{
				//this.product=result.result;
				this.orginal_product=result.result;
				this.product_url=result.url;
				this.orginal_product.forEach((Item)=>{
 				Item['unit_id'] = Item.sk_productbusinessprice[0].pu_unit;
				Item['qty'] = this.getproductqty(Item.product_id,Item.sk_productbusinessprice[0].pu_unit);
					this.product.push(Item);
				});
 				}
								this.test =true;

				loading.dismiss();

				}, (err) => {
								console.log(err);
								loading.dismiss();
								});  
				
				
				}
				
		
	addtocart(p,e)
	{
		let pd = p.sk_productbusinessprice[e];
		pd['qty'] = 1;
		pd['title'] =p.product_name;
		pd['image'] =p.product_image1;
		pd['url'] =this.product_url;

		this.cartdata.push(pd);
        this.setdata(e,p);
		this.services.savecart(this.cartdata);
 	}	
	minustocart(p,e)
	{
		
		let pd = p.sk_productbusinessprice[e];
		//this.cartdata.push(pd);
        //this.setdata(e,p);
		let t =[];
		this.cartdata.forEach((Item)=>{
			if(Item.pu_unit==pd.pu_unit && Item.pu_product_id==pd.pu_product_id && Item.qty>1)
			{
				Item['qty'] =Item['qty']-1;
				t.push(Item);
			}else if(Item.pu_unit==pd.pu_unit && Item.pu_product_id==pd.pu_product_id && Item.qty==1)
			{
				
			}else{
			t.push(Item);	
			}
		});
		this.cartdata=t;
		this.setdata(e,p);
		this.services.savecart(this.cartdata);
 	}	
plustocart(p,e)
	{
		let pd = p.sk_productbusinessprice[e];
		//this.cartdata.push(pd);
        //
		let t =[];
		this.cartdata.forEach((Item)=>{
			if(Item.pu_unit==pd.pu_unit && Item.pu_product_id==pd.pu_product_id)
			{
				Item['qty'] =Item['qty']+1;
				t.push(Item);
			}else{
			t.push(Item);	
			}
		});
		this.cartdata=t;
		this.setdata(e,p);
		this.services.savecart(this.cartdata);
 	}	

	setdata(ev:any,pdd){
console.log(ev,'masddddddd');
console.log(pdd,'masd');
let classname ='clscp'+pdd.product_id;
let classnamed ='clscpadddataunit'+pdd.product_id+''+pdd.sk_productbusinessprice[ev].pu_unit;
let classnamedd ='clscpadddata'+pdd.product_id;

 let x = document.getElementsByClassName(classname);
 let y = document.getElementsByClassName(classnamed);
  let z= document.getElementsByClassName(classnamedd);
//z.addClass('hide');
//y.removeClass('hide');
 console.log(x,"classname");
 
 
 let discountp = (( pdd.sk_productbusinessprice[ev].pu_selling_price - pdd.sk_productbusinessprice[ev].pu_net_price)/pdd.sk_productbusinessprice[ev].pu_selling_price)*100;
 let incart =this.getproductqty(pdd.sk_productbusinessprice[ev].pu_product_id,pdd.sk_productbusinessprice[ev].pu_unit);
 
  x[0].innerHTML='<span class="new-price">₹'+pdd.sk_productbusinessprice[ev].pu_net_price+'</span> <span class="old-price ml-5">₹'+pdd.sk_productbusinessprice[ev].pu_selling_price+'</span> <span class="discount ml-5">'+discountp.toFixed(0)+'% off</span>';
   let t=[];
 	this.orginal_product.forEach((Item)=>{
		           if(Item.product_id==pdd.product_id)
				   {
 				Item['unit_id'] = pdd.sk_productbusinessprice[ev].pu_unit;
				Item['qty'] = this.getproductqty(pdd.product_id,pdd.sk_productbusinessprice[ev].pu_unit);
					t.push(Item);
				   }else{
					 t.push(Item);  
				   }
				});
				this.product =t;
				
}	

		getpercentagediscount(pdd)
		{
		let ev:any = 0;
		 let discountp = (( pdd.sk_productbusinessprice[ev].pu_selling_price - pdd.sk_productbusinessprice[ev].pu_net_price)/pdd.sk_productbusinessprice[ev].pu_selling_price)*100;
		 return discountp.toFixed(0);
 
		}	
	
	getproductqty(pu_product_id,pu_unit_id)
	{
		let t=0;
		this.cartdata.forEach((Item)=>{
			if(Item.pu_product_id==pu_product_id && Item.pu_unit==pu_unit_id)
			{
				t=Item.qty;
			} 
		});
		return t;
	}
	
	getproductobj(product_id)
	{
	let t={};
		this.product.forEach((Item)=>{
			if(Item.product_id==product_id)
			{
				t=Item;
			} 
		});
		return t;	
		
	}
	opencart()
	{
		this.navCtrl.push(MycartPage);
	}
}
