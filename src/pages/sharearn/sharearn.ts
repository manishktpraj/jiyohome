import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Storage } from '@ionic/storage';


/**
* Generated class for the SharearnPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
selector: 'page-sharearn',
templateUrl: 'sharearn.html',
})
export class SharearnPage {
user_data:any={};
user_code:any='';
user_sharemessage:any='';
user_sharescreenmessage:any='';
constructor(public navCtrl: NavController, public navParams: NavParams,private services:ServicesProvider, private socialSharing: SocialSharing,  public loadingCtrl : LoadingController,public storage:Storage){
}
ionViewWillEnter() {
this.storage.get('user_jiyo').then(data => {
if(data!=null)
{

this.user_data= data;	  
this.loadshareingdata();
}
});
}

ionViewDidLoad() {
console.log('ionViewDidLoad ReferPage');
}
  /*
  constructor(public navCtrl: NavController, public navParams: NavParams,private services:ServicesProvider, private socialSharing: SocialSharing,  public loadingCtrl : LoadingController) {
  }

 ionViewWillEnter() {
   	   //this.services.hidetab();
	    this.services.getuser().then((user) => {
		 this.user_data = user; 
		// this.loadshareingdata();
	  }); 
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ReferPage');
  }
  */

loadshareingdata()
{
//let credentials = {};  
//credentials['user_id'] = this.user_data.user_id;

let loading = this.loadingCtrl.create({
content: 'Please wait...'
});

loading.present();

let cred ={user_id:this.user_data.user_id};
console.log(cred);
this.services.sharescreen(cred).then((result: any) => {
console.log(result,"");


loading.dismiss();	
if(result.message=='ok')
{
this.user_sharescreenmessage = 	result.text_share_screen;
this.user_code = 	result.user_share_code;
this.user_sharemessage = result.text_share_message;	
}
}, (err) => {
console.log(err);
loading.dismiss();

});	   

}

shareApp()
{

//  let loading =  this.services._loader_content();
//loading.present();

let loading = this.loadingCtrl.create({
content: 'Please wait...'
});

loading.present();

this.socialSharing.share(this.user_sharemessage, 'JiyoHome').then(() => {
// Sharing via email is possible
loading.dismiss();
}).catch(() => {
// Sharing via email is not possible
loading.dismiss();
});
}
}

