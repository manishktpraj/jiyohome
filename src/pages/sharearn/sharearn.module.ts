import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharearnPage } from './sharearn';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(SharearnPage),
  ],
})
export class SharearnPageModule {}
