import { Component } from '@angular/core';
import { AddressPage } from '../address/address';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { IonicPage, NavController, NavParams,AlertController,ToastController,LoadingController } from 'ionic-angular';

/**
 * Generated class for the AddaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addaddress',
  templateUrl: 'addaddress.html',
})
export class AddaddressPage {

  
  constructor(private services: ServicesProvider ,public navCtrl: NavController, public navParams: NavParams,private toastController: ToastController, private alertcontroller:AlertController, public formbuilder : FormBuilder, public loadingCtrl : LoadingController) {
	 
	  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddaddressPage');
  }
  openaddress()
  {
	  this.navCtrl.push(AddressPage);
  }

}
