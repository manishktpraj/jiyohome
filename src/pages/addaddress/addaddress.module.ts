import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddaddressPage } from './addaddress';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(AddaddressPage),
  ],
})
export class AddaddressPageModule {}
