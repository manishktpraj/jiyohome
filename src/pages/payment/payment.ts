import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../providers/services/services';
import { ThankyouPage } from '../thankyou/thankyou';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var RazorpayCheckout: any;

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
booking_info: any={};
errormessage='';
successmessage='';
transaction_id=0;
user_data:any={};
cart_data:any={};
payble_amount:any=0;
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage,
    public services:ServicesProvider,private loadingCtrl:LoadingController) {
    this.storage.get('user_jiyo').then(data => {
      if(data!=null)
      {
        this.user_data = data;
        }
    });	
    this.cart_data = this.navParams.get('payment_data');
    console.log(this.cart_data,"this.cart_data");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }
  razorPay() {
		let objElement: any = this;
    var options = {
      description: 'On Order',
      image: 'https://www.jiyohome.com/img/logo.png',
      currency: 'INR',
      key: 'rzp_live_FAryCZFvT2PQRC',
      amount:  this.payble_amount*100,
      name: 'Jiyo Home',
	    trans_id:this.transaction_id,
      prefill: {
        email: this.user_data.user_email_id,
        contact: this.user_data.user_mobile,
        name: this.user_data.user_first_name+' '+this.user_data.user_last_name
      },
      theme: {
        color: '#232f3e'
      },
      modal: {
        ondismiss: function() {
          alert('Payment Canceled');
        }
      }
    };

    var successCallback = function(payment_id) {
      /*alert('payment_id: ' + payment_id);*/
	  //objElement.onSaveBooking('razorpay', payment_id);
    objElement.onSaveBooking('razorpay', payment_id);

    };

    var cancelCallback = function(error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
	
	
	 
  }
  openpaymenturl()
  {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
      
       loading.present();
  this.services.createtransactiononcart(this.cart_data).then((result: any) => {
    loading.dismiss();
	 if(result.message=='ok')
				{
            
				this.transaction_id=result.order_id;
        this.payble_amount=result.total_payble;
        this.razorPay();
	
				 
 				}else{
          alert('Server dennied to accept order at this time');
         }
	}, (err) => {
								console.log(err);
						 loading.dismiss();
								});  

  }
  onSaveBooking(strpaymentmethod,payment_id)
  {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
      
  loading.present();
  let cred = {strpaymentmethod:strpaymentmethod,payment_id:payment_id,transaction_id:this.transaction_id};
  this.services.completetransactionpayment(cred).then((result: any) => {
    loading.dismiss();
	 if(result.message!='ok')
				{
          this.navCtrl.push(ThankyouPage);
 				}else{
          alert('Server dennied to accept order at this time');
         }
	}, (err) => {
								console.log(err);
						 loading.dismiss();
								});  
  }
}
