import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';




/**
* Generated class for the OffersPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
selector: 'page-offers',
templateUrl: 'offers.html',
})
export class OffersPage {

	user_data:any={};
	offercelist:any=[];
	from:any='';
	selected_coupon:any={};
	error_message:any='';
	coupon_code:any='';
	cartamount:any=0;
constructor(private services: ServicesProvider, public navCtrl: NavController,private viewCtrl:ViewController,
	 public navParams: NavParams,public storage:Storage,private loadingCtrl:LoadingController) {
	
	if(this.navParams.get('fromd')!=undefined)
	  {
		  this.from = this.navParams.get('fromd');
		  this.cartamount = this.navParams.get('cartamount');
	  }
	 
	this.storage.get('user_jiyo').then(data => {
		if(data!=null)
		{

		this.user_data= data;	  
		this.offer();

		}
	});
}
ionViewDidLoad() {
	console.log('ionViewDidLoad OffersPage');
}
offer()
{
	let loading = this.loadingCtrl.create({
		content: 'Please wait...'
	  });
	  
		loading.present();
	let cred ={user_id:this.user_data.user_id};
	console.log(cred);

	this.services.offer(cred).then((result: any) => {
		console.log(result,"");

		if(result.message!=undefined)
		{
			this.offercelist=result.result;
 			
		}
		loading.dismiss();
	}, (err) => {
								console.log(err);
							 loading.dismiss();
								});  
}
applybyname()
{
	if(this.coupon_code=='')
	{
		this.error_message = 'Please Enter Promocode';
		return false; 
	}
	let loading = this.loadingCtrl.create({
		content: 'Please wait...'
	  });
	  
		loading.present();
    loading.present();
	let data ={user_id:this.user_data.user_id,coupon_code:this.coupon_code,cartamount:this.cartamount};
    this.services.applypromobyname(data).then((result: any) => {
          loading.dismiss();
       
        if(result.message=='ok'){
          
          this.selected_coupon = result;
            this.error_message = '';
         this.closeModal();
      }else{
		  this.error_message = result.notification;
	  }      
    },(error)=>{
      loading.dismiss();
    });
}
applybyid(offerce)
{
	this.coupon_code = offerce.coupon_code;
	this.applybyname();
	
}
closeModal()
{
	this.viewCtrl.dismiss({appliedpromocodeobject:this.selected_coupon});
}

}