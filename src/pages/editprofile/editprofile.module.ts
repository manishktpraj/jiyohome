import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditprofilePage } from './editprofile';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(EditprofilePage),
  ],
})
export class EditprofilePageModule {}
