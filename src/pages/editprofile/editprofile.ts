import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ToastController,LoadingController } from 'ionic-angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the EditprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {
	user_data : any ={};
	loginErrorMsg:any='';
	validations_form:any;
	
  constructor(private services: ServicesProvider ,public navCtrl: NavController,
	 public navParams: NavParams,private toastController: ToastController,
	  private alertcontroller:AlertController, public formbuilder : FormBuilder, public loadingCtrl : LoadingController,public storage:Storage) {
	 
	 
	  
	   this.validations_form = this.formbuilder.group({
	  
	  user_first_name:new FormControl( '', Validators.required),
	  user_last_name:new FormControl( '', Validators.required),
	  user_email_id:new FormControl( '', Validators.required),
	  user_mobile : new FormControl( '', Validators.required)
	
	  });
	  
	   this.storage.get('user_jiyo').then(data => {
	if(data!=null)
	{
      this.user_data = data;
	  console.log(this.user_data,"this.user_data")
	   this.validations_form = this.formbuilder.group({
	  
	  user_first_name:new FormControl(  this.user_data.user_first_name, Validators.required),
	  user_last_name:new FormControl( this.user_data.user_last_name, Validators.required),
	  user_email_id:new FormControl(  this.user_data.user_email_id, Validators.required),
	  user_mobile : new FormControl( this.user_data.user_mobile, Validators.required)
	
	  });
	  
	}
     });
  }

	  checkform(){  
let credentials =  this.validations_form.value;  
let phoneno = /^\d{10}$/;

this.loginErrorMsg = '';
if(credentials['user_first_name']=='')
{
this.loginErrorMsg = 'Please Enter Firstname';
return;
}else if(credentials['user_last_name']=='')
{
this.loginErrorMsg = 'Please Enter Lastname';
return;
}else if(credentials['user_mobile']=='')
{
this.loginErrorMsg = 'Please Enter Mobile Number';
return;
}else if((credentials['user_mobile'].length < 10 || credentials['user_mobile'].length > 10 || !credentials['user_mobile'].match(phoneno))) {
this.loginErrorMsg = "Please enter 10 digit phone number";
return;
}else if(credentials['user_email_id']=='')
{
this.loginErrorMsg = 'Please Enter Email';
return;
}
else{
this.loginErrorMsg = "";

	  
credentials['user_id'] = this.user_data.user_id;


 let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  
    loading.present();
  
 this.services.finaledit(credentials).then((result: any) => {
	 console.log(result,"");
	 
 if(result.message=='ok')
	 
{
	  let editTodoAlert = this.alertcontroller.create({
		title: "Success",
			message:"Profile updated successfully",
			buttons : [{ 
						text: "ok"
				
			}]
			  });
	  editTodoAlert.present()
	 
 //action needed
 	 this.services.saveuser(result.results);
 
}else{
this.loginErrorMsg = 'wrong Username or Password';     
}

loading.dismiss();
}, (err) => {
console.log(err);
loading.dismiss();
});  
}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }

}
