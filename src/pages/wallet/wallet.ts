import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {
	user_data:any={};

walletbalance:any=0;
wallethistory:any=[];
userInfo:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams,private services:ServicesProvider,public storage:Storage,public loadingCtrl : LoadingController) {
  }
ionViewWillEnter() {
/*   	  
	  this.services.hidetab();
	    this.services.getuser().then((user) => {
		 this.userInfo = user; 
		 this.getWallet();
	  }, (err) => {
								console.log(err);
								loading.dismiss();
								});  ); 
	  */
	  	  	this.storage.get('user_jiyo').then(data => {
		if(data!=null)
		{

		this.userInfo= data;	  
		 this.getWallet();

		}
		});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletPage');
  }
 getWallet()
  {
	  let loading = this.loadingCtrl.create({
						content: 'Please wait...'
					  });
	 						loading.present();

	let data ={user_id:this.userInfo.user_id};
	 
    this.services.getWalletHistory(data).then((result: any) => {
          loading.dismiss();
      console.log('result',result);
      if(result.message!=undefined){
        if(result.message=='ok'){
          
          this.walletbalance = result.user_balance;
          this.wallethistory= result.results;
          
        }
      }      
    },(error)=>{
      loading.dismiss();
    });
	 
  }
changedateformat(dated)
{
	//return moment(dated).format('DD,MMM YYYY');
	
}
changetimeformat(dated)
{
	//return moment(dated).format('HH:MM');
	
}
}
