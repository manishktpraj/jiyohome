import { Component } from '@angular/core';
import { IonicPage, NavController,ToastController,AlertController,LoadingController , NavParams,Events} from 'ionic-angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { EditprofilePage } from '../editprofile/editprofile';
import { MyaddressPage } from '../myaddress/myaddress';
import { MyorderPage } from '../myorder/myorder';
import { OffersPage } from '../offers/offers';
import { SharearnPage } from '../sharearn/sharearn';
import { HelpsupportPage } from '../helpsupport/helpsupport';
import { Storage } from '@ionic/storage';
import { MycartPage } from '../mycart/mycart';
import { HomePage } from '../home/home';
import { WalletPage } from '../wallet/wallet';


/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
	
			user_data:any={user_first_name:'',user_last_name:'',user_mobile:''};

  constructor(private services: ServicesProvider, private alertcontroller:AlertController ,public navCtrl: NavController, public navParams: NavParams,private toastController: ToastController,public storage:Storage, public formbuilder : FormBuilder, public loadingCtrl : LoadingController,private events:Events) {
	  
	  			this.storage.get('user_jiyo').then(data => {
				if(data!=null)
				{
				  this.user_data = data;
				  	console.log(this.user_data,"user_data");  
				}
				 });	  
	  
  }
	goToEditPage(){
	  this.navCtrl.push(EditprofilePage)
  }
  
  goToAddressPage(){
	  this.navCtrl.push(MyaddressPage)
  }
  goToOffersPage(){
	  this.navCtrl.push(OffersPage)
  }
  goToShare(){
	  this.navCtrl.push(SharearnPage)
  }
  
  goToOrderPage(){
	  this.navCtrl.push(MyorderPage)
  }
  
  openmycart(){
	  this.navCtrl.push(MycartPage)
  }
	openwallet(){
		  this.navCtrl.push(WalletPage)
	  }
	  opensharearn(){
		  this.navCtrl.push(SharearnPage)
	  }
	  openhelpsupport(){
		  this.navCtrl.push(HelpsupportPage)
	  }
	  
	  

  
  logoutUser(){
	  
let alert = this.alertcontroller.create({
		title: 'Are you sure?',
		message: 'Do you want to logout?',
		buttons: [
		{
		text: 'Cancel',
		role: 'cancel',
		handler: () => {
		}
		
		},
		{
		text: 'Yes',
		handler: () => {
		this.services.saveuser(null);
			   this.events.publish('user_jiyo:created', null, Date.now());

		this.navCtrl.setRoot(HomePage);  
		
		}
		}
		]
		});
		alert.present();
}
 
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

}
