import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController, LoadingController } from 'ionic-angular';
import { MyaddressPage } from '../myaddress/myaddress';
import { OffersPage } from '../offers/offers';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../providers/services/services';

import { PaymentPage } from '../payment/payment';


/**
 * Generated class for the MycartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mycart',
  templateUrl: 'mycart.html',
})
export class MycartPage {
cartdata:any=[];
carttotal:any=0;
cartgrandtotal:any=0;
selected_address:any={};
select_promo:any={};
promo_discount:any=0;
delivery_charge:any=0;
user_data:any={};
min_cart_value:any=0;
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl:ModalController,
	public storage:Storage,private services:ServicesProvider,private loadingCtrl:LoadingController) {
	  
  }
  ionViewWillEnter()
{	
	this.storage.get('user_jiyo').then(data => {
		if(data!=null)
		{
		  this.user_data = data;

		  }
	});	
this.storage.get('user_jiyo_cart').then(data => {
				if(data!=null)
				{
				  this.cartdata = data;
				  console.log( this.cartdata ,"this.cartdata = data ");
				  this.carttotal = this.getproducttotal();
				  this.cartgrandtotal =  this.carttotal ;
				  this.getcartdetail();
  				}
				 });	
				 
				 this.getcartdetail();

	
}  
getcartdetail()
{
	let loading = this.loadingCtrl.create({
		content: 'Please wait...'
	  });
		
		 loading.present();
		 let cred = {};
	this.services.getcartinfo(cred).then((result: any) => {
	  loading.dismiss();
	   if(result.message=='ok')
				  {
			  this.min_cart_value = result.minorder;
 				   
				   } 
	  }, (err) => {
								  console.log(err);
						   loading.dismiss();
								  });  

}
minustocart(p)
{
	
	let pd = p;
	//this.cartdata.push(pd);
	//this.setdata(e,p);
	let t =[];
	this.cartdata.forEach((Item)=>{
		if(Item.pu_unit==pd.pu_unit && Item.pu_product_id==pd.pu_product_id && Item.qty>1)
		{
			Item['qty'] =Item['qty']-1;
			t.push(Item);
		}else if(Item.pu_unit==pd.pu_unit && Item.pu_product_id==pd.pu_product_id && Item.qty==1)
		{
			
		}else{
		t.push(Item);	
		}
	});
	this.cartdata=t;
	 this.services.savecart(this.cartdata);
	 this.carttotal = this.getproducttotal();	 
	 this.cartcalculation();
 }	
 plustocart(p)
	{
		let pd = p;
		//this.cartdata.push(pd);
        //
		let t =[];
		this.cartdata.forEach((Item)=>{
			if(Item.pu_unit==pd.pu_unit && Item.pu_product_id==pd.pu_product_id)
			{
				Item['qty'] =Item['qty']+1;
				t.push(Item);
			}else{
			t.push(Item);	
			}
		});
		this.cartdata=t;
		 this.services.savecart(this.cartdata);
		 this.carttotal = this.getproducttotal();	 
		 this.cartcalculation();

	 }
	 
	 
			
	getproductqty(pu_product_id,pu_unit_id)
	{
		let t=0;
		this.cartdata.forEach((Item)=>{
			if(Item.pu_product_id==pu_product_id && Item.pu_unit==pu_unit_id)
			{
				t=Item.qty;
			} 
		});
		return t;
	}
	
	 
  ionViewDidLoad() {
    console.log('ionViewDidLoad MycartPage');
  }

  openoffers()
  {
    
	  //console.log(this.cart_grandtotal);
   let profileModal = this.modalCtrl.create(OffersPage, { fromd: 'cart' ,cartamount:this.cartgrandtotal}
    );
	  profileModal.onDidDismiss(data => {
		  console.log(data,"applied data");
		  if(data.appliedpromocodeobject!=undefined && data.appliedpromocodeobject!=null)
		  {
     
	 this.select_promo =data.appliedpromocodeobject; 
	 if(this.select_promo.discount_amount!=undefined)
	 {
		this.promo_discount = this.select_promo.discount_amount.toFixed(0);

	 }
      this.cartcalculation();
		  }
   });
      profileModal.present(); 
  
    }
  cartcalculation()
  {

	this.cartgrandtotal = this.carttotal+this.delivery_charge-this.promo_discount;
  }
    openaddress()
{
 	 let profileModal = this.modalCtrl.create(MyaddressPage, { fromd: 'cart'});
	  profileModal.onDidDismiss(data => {
		  if(data!=undefined && data!=null)
		  {
             this.selected_address = data.selected_address;
			 console.log(this.selected_address,"this.selected_address");
			 if(this.selected_address.shipping!=undefined)
			 {
			this.delivery_charge = 	this.selected_address.shipping.shipping_rate;
			 }
			 this.cartcalculation();

		  }
   });
      profileModal.present(); 
	  
	
}
getproducttotal()
	{
		let t=0;
		this.cartdata.forEach((Item)=>{
			t += (Item.qty*Item.pu_net_price)
		});
		return t;
	}
	removepromo()
	{
		this.select_promo ={}; 
		this.promo_discount =0;
		this.cartcalculation();
	}	
openpayment()
{
let cred = {user_id:this.user_data.user_id,cart_data:this.cartdata,grand_total:this.cartgrandtotal,
	promo_discount:this.promo_discount,delivery_charge:this.delivery_charge,abdata:this.selected_address,
	coupon_id:this.select_promo.coupon_id
};
this.navCtrl.push(PaymentPage,{payment_data:cred});
}

}
