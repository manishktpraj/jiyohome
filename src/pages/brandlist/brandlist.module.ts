import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrandlistPage } from './brandlist';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(BrandlistPage),
  ],
})
export class BrandlistPageModule {}
