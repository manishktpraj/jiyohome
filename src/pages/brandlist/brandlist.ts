import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { ProductdetailsPage } from '../productdetails/productdetails';


/**
 * Generated class for the BrandlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-brandlist',
  templateUrl: 'brandlist.html',
})
export class BrandlistPage {
	category:any={};
	product:any=[];
	product_url:any='';
	test:boolean =false;

	
  constructor(private services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams,public loadingCtrl : LoadingController) {
	  this.category=this.navParams.get('category');
		console.log(this.category);
	 this.brandlisting();
	  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductlistPage');
  }

	brandlisting()
				{
					let loading = this.loadingCtrl.create({
						content: 'Please wait...'
					  });
					  
						loading.present();
					let cred ={brand_id:this.category.brand_id};
					console.log(cred);
					
						this.services.brandlisting(cred).then((result: any) => {
				console.log(result,"");
				
				if(result.message!=undefined)
				{
				this.product=result.result;
				this.product_url=result.url;
				}
									this.test =true;
							loading.dismiss();

				}
								, (err) => {
								console.log(err);
								loading.dismiss();
								}); 
				}
				
		openproductdetail(product)
			{
				this.navCtrl.push(ProductdetailsPage,{product_id:product.product_id});
			}

			getpercentagediscount(pdd)
			{
		let ev:any = 0;
		 let discountp = (( pdd.sk_productbusinessprice[ev].pu_selling_price - pdd.sk_productbusinessprice[ev].pu_net_price)/pdd.sk_productbusinessprice[ev].pu_selling_price)*100;
		 return discountp.toFixed(0);
 
			}	
			
			setdata(ev:any,p){
				console.log(ev,'masd');
				console.log(p,'masd');
				let classname ='clscp'+p.product_id;
				 let x = document.getElementsByClassName(classname);
				 console.log(x,"classname");
				 
				 
				 let discountp = (( p.sk_productbusinessprice[ev].pu_selling_price - p.sk_productbusinessprice[ev].pu_net_price)/p.sk_productbusinessprice[ev].pu_selling_price)*100;
				 
				x[0].innerHTML='<span class="new-price">₹'+p.sk_productbusinessprice[ev].pu_net_price+'</span> <span class="old-price ml-5">₹'+p.sk_productbusinessprice[ev].pu_selling_price+'</span> <span class="discount ml-5">'+discountp.toFixed(0)+'% off</span>';	
				}			
			
}
