import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  user_data : any ={};
  product:any=[];
	product_url:any='';


  constructor(private services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams,public loadingCtrl : LoadingController,public storage:Storage) {
  
    this.storage.get('user_jiyo').then(data => {
      if(data!=null)
      {
  
      this.user_data= data;	  
      this.shownotification();
  
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

  shownotification(){


    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
      
       loading.present();
              
      let cred ={user_id:this.user_data.user_id};
      console.log(cred);
    
      this.services.notifications(cred).then((result: any) => {
  console.log(result,"");
  
  if(result.message!=undefined)
  {
    if(result.result!=undefined)
    {
      this.product=result.result;
      this.product_url=result.url;
    }

  }
   loading.dismiss();

  }, (err) => {
          console.log(err);
          loading.dismiss();
          });  
  
  
  }

}
