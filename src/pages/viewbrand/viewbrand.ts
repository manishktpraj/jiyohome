import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController} from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { BrandlistPage } from '../brandlist/brandlist';



/**
 * Generated class for the ViewbrandPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewbrand',
  templateUrl: 'viewbrand.html',
})
export class ViewbrandPage {
	
		credentials : any = {};
		dashboarddata:any=[];
url:any='';
  constructor(private services: ServicesProvider,public navCtrl: NavController, public navParams: NavParams,private loadingCtrl:LoadingController) {
	  
	  
	  
	this.getbrandlist();
	  
  }
  
  openbrandlist(category)
			{
			this.navCtrl.push(BrandlistPage,{category:category});
			}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewbrandPage');
  }
  getbrandlist()
  {
	  	let loading = this.loadingCtrl.create({
						content: 'Please wait...'
					  });
					  
		loading.present();
	    this.services.brands(this.credentials).then((result: any) => {
				console.log(result,"");
				
								
				if(result.message!=undefined)
				{
 
								 this.dashboarddata=result.result;
								 this.url=result.url;
 
				}
										loading.dismiss();

				}, (err) => {
								console.log(err);
								loading.dismiss();
								});  
  }

}
