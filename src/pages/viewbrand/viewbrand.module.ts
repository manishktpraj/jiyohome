import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewbrandPage } from './viewbrand';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ViewbrandPage),
  ],
})
export class ViewbrandPageModule {}
