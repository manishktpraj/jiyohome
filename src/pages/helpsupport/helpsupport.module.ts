import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpsupportPage } from './helpsupport';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(HelpsupportPage),
  ],
})
export class HelpsupportPageModule {}
