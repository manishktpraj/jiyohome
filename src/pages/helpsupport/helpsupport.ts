import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { FaqPage } from '../faq/faq';


/**
 * Generated class for the HelpsupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-helpsupport',
  templateUrl: 'helpsupport.html',
})
export class HelpsupportPage {
staticPages:any= {staticPages:'info@jiyohome.com',support_call:'7014702933'};

  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl : LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpsupportPage');
  }
  
  onShowFaq(add)
  {
	  let loading = this.loadingCtrl.create({
						content: 'Please wait...'
					  });
					  
						loading.present();
	  this.navCtrl.push(FaqPage);
	  				loading.dismiss();

  }

}
