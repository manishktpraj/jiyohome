import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductlistPage } from '../productlist/productlist';
import { ServicesProvider } from '../../providers/services/services';


/**
 * Generated class for the ViewonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewone',
  templateUrl: 'viewone.html',
})
export class ViewonePage {
	
	credentials : any = {};
	dashboarddata:any={};

  constructor(private services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams) {
	  
	  		this.services.homedashboard(this.credentials).then((result: any) => {
				console.log(result,"");
				this.dashboarddata=result;
				}, (err) => {
								console.log(err);
								//loading.dismiss();
								}); 
  
  }
  
  openproductlist(category)
			{
			this.navCtrl.push(ProductlistPage,{category:category});
			}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewonePage');
  }
  
  

}
