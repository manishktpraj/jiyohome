import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewonePage } from './viewone';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ViewonePage),
  ],
})
export class ViewonePageModule {}
