import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
 


/**
 * Generated class for the CityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-city',
  templateUrl: 'city.html',
})
export class CityPage {
state_name:any='';	
city_list:any=[];
prelooaded_city:any=[];
selectedcityid:any={};

   constructor(private services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams,
	public loadingCtrl : LoadingController,public viewCtrl: ViewController)  {
		this.state_name =this.navParams.get('state_data');
		this.getcitylist();
  }
  selectSearchResult(item)
  {
	  this.selectedcityid=item.cities_name;
	  this.dismiss();
  }
  
  dismiss() {
   this.viewCtrl.dismiss(this.selectedcityid);
  }




  getcitylist()
  {

	    let cred ={state_name:this.state_name};
		let loading = this.loadingCtrl.create({
			content: 'Please wait...'
		  });
			loading.present();  
 this.services.citylist(cred).then((result: any) => {
	 console.log(result,"");
	 loading.dismiss();
	 if(result.message!=undefined)
				{ 
					
					this.city_list=result.result;
					this.prelooaded_city=result.result;
					console.log(this.city_list,"this.city_list");

				 }
				else{
				this.city_list=[];	
					
				
				}

	  
	}, (err) => {
								console.log(err);
						 	loading.dismiss();
								});   
  }

  getItems(ev: any) 
  {
	   let searchdata:any= ev.target.value;
	   this.city_list=[];
	   this.prelooaded_city.forEach( (item, index)=> {
								  if(item.cities_name.toUpperCase().indexOf(searchdata.toUpperCase()) > -1) {
									   this.city_list.push(item);
								 }
							 
						 });
	   
   }
   onCancel()
   {
	   this.city_list=this.prelooaded_city;
	   
	   
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CityPage');
  }

}
