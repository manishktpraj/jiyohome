import { Component } from '@angular/core';
import { IonicPage, NavController,ToastController,LoadingController , NavParams} from 'ionic-angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { OtpPage } from '../otp/otp';
import { HomePage } from '../home/home';
import { TermconditionPage } from '../termcondition/termcondition';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the RegisterphonenumberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registerphonenumber',
  templateUrl: 'registerphonenumber.html',
})
export class RegisterphonenumberPage {
	 loginErrorMsg:any='';
	
	validations_form:any;
	

   constructor(private services: ServicesProvider ,public navCtrl: NavController, public navParams: NavParams,private toastController: ToastController, public formbuilder : FormBuilder, public loadingCtrl : LoadingController,public storage:Storage) {
  this.storage.get('user_jiyo').then(data => {
	if(data!=null)
	{
      this.navCtrl.setRoot(HomePage);
	}
     });
   this.validations_form = this.formbuilder.group({
	  
	 
	 user_mobile : new FormControl( '', Validators.required)
  });
  
  
  }
  
  sendotp(){  
let credentials =  this.validations_form.value;  
 let phoneno = /^\d{10}$/;

this.loginErrorMsg = '';
 console.log(credentials,"credentials");
 
if(credentials['user_mobile']=='')
{
this.loginErrorMsg = 'Please Enter Mobile Number';
return;
}else if((credentials['user_mobile'].length < 10 || credentials['user_mobile'].length > 10 || !credentials['user_mobile'].match(phoneno))) {
this.loginErrorMsg = "Please enter 10 digit phone number";
return;
}
else{
this.loginErrorMsg = "";
	
	
	let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  
    loading.present();
  
 this.services.sendotp(credentials).then((result: any) => {
	 console.log(result,"");
	 

	 if(result.message=='ok')
{
	
 //action needed
 this.navCtrl.push(OtpPage, {user_mobile:credentials['user_mobile'],user_id:result.user_id})
}
else{
this.loginErrorMsg = 'wrong Username or Password';     
}

loading.dismiss();
}, (err) => {
console.log(err);
loading.dismiss();
});  

}

}
   goToHome(){
  this.navCtrl.setRoot(HomePage)
  }

  goTotnc(){
	 
  this.navCtrl.push(TermconditionPage)
	  			 
  }
 
  
  /*
checkmobilenumber(){  
let credentials =  this.validations_form.value;  
this.loginErrorMsg = '';
if(credentials['num']=='')
{
this.loginErrorMsg = 'Please Enter Mobile Number';
return;
}

 let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  
    loading.present();
  
 this.services.loginuser(credentials).then((result: any) => {
	 console.log(result,"");
 if(result.message=='ok')
{
 //action needed
}else{
this.loginErrorMsg = 'please enter 10 digit mobile number';     
}

loading.dismiss();
}, (err) => {
console.log(err);
loading.dismiss();
});  

}
*/
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterphonenumberPage');
  }

}
