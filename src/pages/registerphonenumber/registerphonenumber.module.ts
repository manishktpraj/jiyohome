import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterphonenumberPage } from './registerphonenumber';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(RegisterphonenumberPage),
  ],
})
export class RegisterphonenumberPageModule {}
