import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';

import { ProductlistPage } from '../productlist/productlist';
import { MycartPage } from '../mycart/mycart';

/**
 * Generated class for the ProductdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productdetails',
  templateUrl: 'productdetails.html',
})
export class ProductdetailsPage {
	
	product_id:any=0;
	related_product:any=[];
	product_detail:any={};

	product_url:any='';
	test:boolean =false;
	user_data:any={user_id:0};
	main_category:any={};
	avail:any= {};
	unitSelectedId:any='';
	unitSelectedObject:any={};
	outofstock:any='In Stock';
	cartdata:any=[];

  constructor(private services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams,public loadingCtrl : LoadingController,public storage:Storage) {
	  
	    this.product_id=this.navParams.get('product_id');
		this.storage.get('user_jiyo_cart').then(data => {
			if(data!=null)
			{
			  this.cartdata = data;
			 
			  }
			 });	  
  }
  getdetail(pddd)
  {
	   this.product_id= pddd.product_id;
	   this.productdetail();
  }
  ionViewWillEnter()
  {
	   				this.productdetail();

	  	this.storage.get('user_jiyo').then(data => {
		if(data!=null)
		{

		this.user_data= data;	  

		}
		});
  }
  
   productdetail()
				{
					let loading = this.loadingCtrl.create({
						content: 'Please wait...'
					  });
					  
						loading.present();
						let cred ={};		
					cred['product_id'] =this.product_id;
					cred['user_id'] =this.user_data.user_id;
					
 					
						this.services.products(cred).then((result: any) => {
				console.log(result,"");
				
				if(result.message!=undefined)
				{
			    this.related_product=result.relatedproduct;
			    this.product_detail=result.result;
			 	this.product_url=result.product_url;
			 	this.main_category=result.maincategorydetail;
		  this.unitSelectedId = this.product_detail.child[0].unit_id;
		  this.unitSelectedObject = this.product_detail.child[0];
		  if(this.unitSelectedObject.pu_qty>0)
		  {
		this.outofstock ='In Stock';
		  }else{
			this.outofstock ='Out Of Stock';
		  
		  }		
				}
								this.test =true;

				loading.dismiss();

				}, (err) => {
								console.log(err);
								loading.dismiss();
								});  
				
				
				}
				
  
	setUnitData(unitdata)
{
	if(unitdata.pu_qty>0)
	{
this.unitSelectedId =unitdata.unit_id;
this.unitSelectedObject =unitdata;	
 if(this.unitSelectedObject.pu_qty>0)
		  {
		this.outofstock ='In Stock';
		  }else{
			this.outofstock ='Out Of Stock';
		  
		  }		
	}
}
		
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductdetailsPage');
  }
  goViewproductdetail()
			{
			this.navCtrl.push(ProductlistPage,{category:this.main_category});
			}


getpercentagediscount(pdd)
	{
	 
		 let discountp = (( pdd.pu_selling_price - pdd.pu_net_price)/pdd.pu_selling_price)*100;
		 return discountp.toFixed(0);
 
	}
	getpercentagediscountw(pdd)
	{
		let ev:any = 0;
		 let discountp = (( pdd.sk_productbusinessprice[ev].pu_selling_price - pdd.sk_productbusinessprice[ev].pu_net_price)/pdd.sk_productbusinessprice[ev].pu_selling_price)*100;
		 return discountp.toFixed(0);
 
	}
	getpercentagediscountww()
	{
	 
		 let discountp = (( this.unitSelectedObject.pu_selling_price - this.unitSelectedObject.pu_net_price)/this.unitSelectedObject.pu_selling_price)*100;
		 return discountp.toFixed(0);
 
	}
	buynow()
	{
		let loading = this.loadingCtrl.create({
			content: 'Please wait...'
		  });
		  
			loading.present();
			let isexist = this.getproductqty(this.unitSelectedObject.pu_product_id,this.unitSelectedObject.pu_unit);
			if(isexist>0)
			{
				let t=[];
				this.cartdata.forEach((Item)=>{
					if(Item.pu_product_id==this.unitSelectedObject.pu_product_id && Item.pu_unit==this.unitSelectedObject.pu_unit)
					{
						Item['qty'] =Item['qty']+1; 
						t.push(Item);
					}else{

						t.push(Item);
					} 
				});
				 
				this.cartdata = t;
				this.services.savecart(t);
				loading.dismiss();
			}else{
				let pd = this.unitSelectedObject;
				pd['qty'] = 1;
				pd['title'] =this.product_detail.product_name;
				pd['image'] =this.product_detail.product_image1;
				pd['url'] =this.product_url;
		
				this.cartdata.push(pd);
				 this.services.savecart(this.cartdata);
				 loading.dismiss();
			}
this.navCtrl.push(MycartPage);
	}
	addtocart()
	{
		let loading = this.loadingCtrl.create({
			content: 'Please wait...'
		  });
		  
			loading.present();
			let isexist = this.getproductqty(this.unitSelectedObject.pu_product_id,this.unitSelectedObject.pu_unit);
			if(isexist>0)
			{
				let t=[];
				this.cartdata.forEach((Item)=>{
					if(Item.pu_product_id==this.unitSelectedObject.pu_product_id && Item.pu_unit==this.unitSelectedObject.pu_unit)
					{
						Item['qty'] =Item['qty']+1; 
						t.push(Item);
					}else{

						t.push(Item);
					} 
				});
				 
				this.cartdata = t;
				this.services.savecart(t);
				loading.dismiss();
			}else{
				let pd = this.unitSelectedObject;
				pd['qty'] = 1;
				pd['title'] =this.product_detail.product_name;
				pd['image'] =this.product_detail.product_image1;
				pd['url'] =this.product_url;
		
				this.cartdata.push(pd);
				 this.services.savecart(this.cartdata);
				 loading.dismiss();
			}
	}
	opencart()
	{

	
		this.navCtrl.push(MycartPage);


	}
	getproductqty(pu_product_id,pu_unit_id)
	{
		let t=0;
		this.cartdata.forEach((Item)=>{
			if(Item.pu_product_id==pu_product_id && Item.pu_unit==pu_unit_id)
			{
				t=Item.qty;
			} 
		});
		return t;
	}
}
