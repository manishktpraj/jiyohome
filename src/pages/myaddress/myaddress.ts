import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ViewController,ToastController,LoadingController } from 'ionic-angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { HomePage } from '../home/home';
import { AddressPage } from '../address/address';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the MyaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myaddress',
  templateUrl: 'myaddress.html',
})
export class MyaddressPage {
	
 	  loginErrorMsg:any='';
	  user_data : any ={};

		// category:any={};
		product:any=[];
		product_url:any='';
selected_address:any={};
	from:any='';
	validations_form:any;
	states_list:any=[];

	
  constructor(private services: ServicesProvider ,public navCtrl: NavController, public navParams: NavParams,private toastController: ToastController, private alertcontroller:AlertController, public formbuilder : FormBuilder, public loadingCtrl : LoadingController,public storage:Storage,private viewCtrl:ViewController) {
	 
	 
 	  
	if(this.navParams.get('fromd')!=undefined)
	{
		this.from =this.navParams.get('fromd');
	}		
	 

  }
  
 
  
  ionViewWillEnter()
  {
	  
	  	this.storage.get('user_jiyo').then(data => {
		if(data!=null)
		{

		this.user_data= data;	  
		this.getaddress();

		}
		});
  }
  getaddress()
  {
	let loading = this.loadingCtrl.create({
		content: 'Please wait...'
	  });
		
		 loading.present();
	    let cred ={ab_user_id:this.user_data.user_id};
					console.log(cred);
  
 this.services.getaddress(cred).then((result: any) => {
	 console.log(result,"");
	 loading.dismiss();

	 if(result.message!=undefined)
				{ if(result.result!=undefined)
				{
				this.product=result.result;
				this.product_url=result.url;
		
				}else{
				this.product=[];	
					
				}
				this.states_list = result.states;
				console.log(this.states_list,'this.states_list');
				}

	  
	}, (err) => {
								console.log(err);
								 loading.dismiss();
								});  
  }
  openaddress()
  {
	  this.navCtrl.push(AddressPage,{states_list:this.states_list});
  }
  
  editaddress(add)
  {
	  	  this.navCtrl.push(AddressPage,{address_data:add,states_list:this.states_list});


  }
  
  
  
  deleteadd(add)
  {  
  let alert = this.alertcontroller.create({
		title: 'Are you sure?',
		message: 'Do you want to Delete?',
		buttons: [
		{
		text: 'Cancel',
		role: 'cancel',
		handler: () => {
		}
		
		},
		{
		text: 'Yes',
		handler: () => {
			
		
	   let addr = {};

	   addr['ab_id'] = add.ab_id;
	   addr ['ab_user_id'] = add.ab_user_id;
					console.log(addr);
	  
	this.services.removeaddress(addr).then((result: any) => {
		console.log(result,"");
		
				 this.getaddress();
				 		
	});  
	
		}
		}
		]
		});
		alert.present();
		


  }
  
  closeModal()
  {
	  	this.viewCtrl.dismiss({selected_address:this.selected_address});

  }

selectaddress(addd)
{
	if(this.from!='')
	{
		this.selected_address = addd;
		this.closeModal();
	}

}
  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }
}
