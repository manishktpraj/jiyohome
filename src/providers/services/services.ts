import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import {ToastController, AlertController} from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

const API_URL: String =' https://codexosoftware.com/demo/grocery/webapi/';
@Injectable()


export class ServicesProvider {
	
 API_URL ='https://codexosoftware.com/demo/grocery/webapi/';
 APP_VERSION_CODE:any=1.0;

  constructor(public http: HttpClient,private storage:Storage,private toastCtrl:ToastController,
    private alertCtrl:AlertController,private inapp:InAppBrowser) {
    console.log('Hello ServicesProvider Provider');
    this.getupdatepopup();

  }
  getupdatepopup()
  {
    let data={};
    this.getappversion(data).then((result: any) => {
       if(result.version>this.APP_VERSION_CODE)
      {
        this.showUpdatePopup(result.version);
      }
    });
  }
  showUpdatePopup(versioncode)
  {

    let  alert =    this.alertCtrl.create({
      title: 'New Version',
      message: versioncode+' version Available on play store.Please update app',
      buttons: [
        {
          text: 'UPDATE',
          role: 'cancel',
          handler: () => {
            this.showUpdatePopup(versioncode)
             this.inapp.create('https://play.google.com/store/apps/details?id=jiyohome.codexo&hl=en_IN','_system');
          }
        }

      ]
    });
    alert.present();
  }
 
  getappversion(data:any){
    return new Promise((resolve, reject) => {
          this.http.post(this.API_URL+'dashboard/getappversion', data)
            .subscribe(res => {
              resolve(res);
            },
        (err) => {
              reject(err);
            });
        });
      }
  loginuser(data:any){
  return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'register/loginUser', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	sendotp(data:any){
  return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'register/', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	otpcheck(data:any){
  return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'register/verifyotp/', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	 finalregis(data:any){
  return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'register/finalregis/', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
    presentToast(strmessage) {
      let toast = this.toastCtrl.create({
        message: strmessage,
        duration: 1000,
        position: 'bottom'
      });
    
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
    
      toast.present();
    }
    
	 finaledit(data:any){
  return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'register/updateuserprofile', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	saveuser(data)
	{
		this.storage.set('user_jiyo',data);
		return true;
	}
	savecart(data)
	{
		this.storage.set('user_jiyo_cart',data);
		return true;
	}
	getcart()
	{
	return new Promise(resolve => {
     this.storage.get('user_jiyo_cart').then(data => {
       resolve(data);
     });
   });
	}
	getuser()
	{
	return new Promise(resolve => {
     this.storage.get('user_jiyo').then(data => {
       resolve(data);
     });
   });
	}
	/*
	sharescreen(data) {
    return new Promise((resolve, reject) => {
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      const requestOptions = new RequestOptions({headers: headers});
      this.http.post(this.API_URL + '/share-screen.php', data, requestOptions)
        .subscribe(res => {
          resolve((res));
        }, (err) => {
         // resolve(this._handleError(err, false));
        });
    });
  }  
*/
   
    /*
    public _loader_content() {
    let loading = this.loader.create({
      spinner: 'hide',
      content: '<img src="../../assets/imgs/logo.png" class="loader" ><br><img src="../../assets/imgs/loader.gif" class="loader" >',
      duration: 10000000
    });
    return loading;
  }
*/
createtransactiononcart(data){
  return new Promise((resolve, reject) => {
      this.http.post(this.API_URL+'orders/createTransactionOnCartId', data)
        .subscribe(res => {
          resolve(res);
        },
    (err) => {
          reject(err);
        });
    });
  }
  getcartinfo(data){
    return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'cart/', data)
          .subscribe(res => {
            resolve(res);
          },
      (err) => {
            reject(err);
          });
      });
    }
  completetransactionpayment(data){
    return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'orders/completetransactionpayment', data)
          .subscribe(res => {
            resolve(res);
          },
      (err) => {
            reject(err);
          });
      });
    }
	sharescreen(data){
		return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'register/sharetext', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	getWalletHistory(data){
		return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'dashboard/wallethistory', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }


	/*
	sharescreen(data) {
    return new Promise((resolve, reject) => {
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      const requestOptions = new RequestOptions({headers: headers});
      this.http.post(this.API_URL + '/register/sharetext', data, requestOptions)
        .subscribe(res => {
          resolve((res));
        }, (err) => {
          resolve(this._handleError(err, false));
        });
    });
  }  
    public _loader_content() {
    let loading = this.loader.create({
      spinner: 'hide',
      content: '<img src="../../assets/imgs/logo.png" class="loader" ><br><img src="../../assets/imgs/loader.gif" class="loader" >',
      duration: 10000000
    });
    return loading;
  }
*/

	 homedashboard(data:any){
		return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'dashboard', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	viewcategory(data:any){
		return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'dashboard/gettagproduct', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	 getparentcategory(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'dashboard/getparentcategory', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
		 productcategory(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'category/categoryproduct/', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	 brandlisting(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'category/brandproduct', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    } 
    
    brands(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'category/brand', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	 offer(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'dashboard/offerce', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }

	 getaddress(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'addressbook/', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	
	 saveaddress(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'addressbook/addaddress', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	 removeaddress(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'addressbook/removeaddress', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
    
    applypromobyname(data:any){
      return new Promise((resolve, reject) => {
            this.http.post(this.API_URL+'cart/applypromocode', data)
              .subscribe(res => {
                resolve(res);
              },
          (err) => {
                reject(err);
              });
          });
        }
        orders(data:any){
          return new Promise((resolve, reject) => {
                this.http.post(this.API_URL+'orders/', data)
                  .subscribe(res => {
                    resolve(res);
                  },
              (err) => {
                    reject(err);
                  });
              });
            }
	citylist(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'addressbook/citylist', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	statelist(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'addressbook/statelist', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }
	
	products(data:any){
	return new Promise((resolve, reject) => {
        this.http.post(this.API_URL+'product/', data)
          .subscribe(res => {
            resolve(res);
          },
		  (err) => {
            reject(err);
          });
      });
    }

    notifications(data:any){
      return new Promise((resolve, reject) => {
            this.http.post(this.API_URL+'faq/notification', data)
              .subscribe(res => {
                resolve(res);
              },
          (err) => {
                reject(err);
              });
          });
        }

        mngorders(data:any){
          return new Promise((resolve, reject) => {
                this.http.post(this.API_URL+'faq/notification', data)
                  .subscribe(res => {
                    resolve(res);
                  },
              (err) => {
                    reject(err);
                  });
              });
            }
    
        

}
